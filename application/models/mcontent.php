<?php
class Mcontent extends CI_Model{
	//add new tracking banner
	private $tcontent = 'TCONTENT';
	
	function Content(){
		parent::Model();
	}
	
	function getContent(){
		$q = $this->db->query("	SELECT *
								FROM TCONTENT
								ORDER BY CONTENTDATE DESC
								LIMIT 17
								");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}
	
	function getContentFULL(){
		$q = $this->db->query("	SELECT *
								FROM TCONTENT
								ORDER BY CONTENTDATE DESC
								");
		if($q->num_rows()>0){
			return $q->result();
		}
		return array();
	}
	
	function getGallery() {
        $this->db->select('TCONTENT.*');
		$this->db->from('TCONTENT');
		$this->db->where('TCONTENT.CONTENTTYPE','images');
		$this->db->order_by('TCONTENT.CONTENTDATE','DESC');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
	
	function saveGallery($datagallery){
		$this->db->insert('TCONTENT', $datagallery);
	}
	
	function deleteGallery($id){
		$this->db->delete('TCONTENT', array('CONTENTID' => $id)); 
	}
	function getVideo() {
        $this->db->select('TCONTENT.*');
		$this->db->from('TCONTENT');
		$this->db->where('TCONTENT.CONTENTTYPE','videos');
		$this->db->order_by('TCONTENT.CONTENTDATE','DESC');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
	function saveVideo($datavideo){
		$this->db->insert('TCONTENT', $datavideo);
	}
	
	function deleteVideo($id){
		$this->db->delete('TCONTENT', array('CONTENTID' => $id)); 
	}
	
	function getScheduleHome() {
        $this->db->select('TSCHEDULE.*');
		$this->db->from('TSCHEDULE');
		$this->db->order_by('TSCHEDULE.SCHEDULEDATE','DESC');
		$this->db->limit(5);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
	
	function getSchedule() {
        $this->db->select('TSCHEDULE.*');
		$this->db->from('TSCHEDULE');
		$this->db->order_by('TSCHEDULE.SCHEDULEDATE','DESC');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
	function saveSchedule($dataschedule){
		$this->db->insert('TSCHEDULE', $dataschedule);
	}
	
	function deleteSchedule($id){
		$this->db->delete('TSCHEDULE', array('SCHEDULEID' => $id)); 
	}
	
	function getPlaylistHome() {
        $this->db->select('TPLAYLIST.*');
		$this->db->from('TPLAYLIST');
		$this->db->WHERE('TPLAYLIST.PLAYLISTFEATURED','1');
		$this->db->order_by('TPLAYLIST.PLAYLISTDATE','DESC');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
	
	function getPlaylist() {
        $this->db->select('TPLAYLIST.*');
		$this->db->from('TPLAYLIST');
		$this->db->order_by('TPLAYLIST.PLAYLISTDATE','DESC');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
	
	function getEditPlaylist($playlistID) {
        $this->db->select('TPLAYLIST.*');
		$this->db->from('TPLAYLIST');
		$this->db->where('TPLAYLIST.PLAYLISTID',$playlistID);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
	function updatePlaylist($id, $dataplaylist) {
		$this->db->where('PLAYLISTID', $id);
		$result = $this->db->update('TPLAYLIST', $dataplaylist); 
		return $result;
    }
	function savePlaylist($dataplaylist){
		$this->db->insert('TPLAYLIST', $dataplaylist);
	}
	
	function deletePlaylist($id){
		$this->db->delete('TPLAYLIST', array('PLAYLISTID' => $id)); 
	}
	
	function getStory() {
        $this->db->select('TSTORY.*');
		$this->db->from('TSTORY');
		$this->db->order_by('TSTORY.STORYDATE','DESC');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
	
	function getApelStory($slug) {
        $this->db->select('TSTORY.*');
		$this->db->from('TSTORY');
		$this->db->where('TSTORY.STORYSLUG',$slug);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
	
	function saveStory($datastory){
		$this->db->insert('TSTORY', $datastory);
	}
	function getEditStory($storyID) {
        $this->db->select('TSTORY.*');
		$this->db->from('TSTORY');
		$this->db->where('TSTORY.STORYID',$storyID);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
	function updateStory($id, $datastory) {
		$this->db->where('STORYID', $id);
		$result = $this->db->update('TSTORY', $datastory); 
		return $result;
    }
	function deleteStory($id){
		$this->db->delete('TSTORY', array('STORYID' => $id)); 
	}
}
/*
CREATE TABLE TUSER(
	USERID INT NOT NULL AUTO_INCREMENT,
	USERNAME VARCHAR(255),
	USERPASSWORD VARCHAR(255),
	USERLEVEL INT,
	PRIMARY KEY (USERID)
);

INSERT INTO TUSER (USERNAME,USERPASSWORD,USERLEVEL) VALUES ('admin','apelbandadmin2015.!',1);

CREATE TABLE TSCHEDULE(
	SCHEDULEID INT NOT NULL AUTO_INCREMENT,
	SCHEDULETITLE VARCHAR(255),
	SCHEDULEDATE DATE,
	PRIMARY KEY (SCHEDULEID)
);
INSERT INTO TSCHEDULE (SCHEDULETITLE, SCHEDULEDATE) VALUES ('Kamera Ria - TVRI','2014-12-16');
								INSERT INTO TSCHEDULE (SCHEDULETITLE, SCHEDULEDATE) VALUES ('Kasih Ibu - TVRI','2014-12-22');
								INSERT INTO TSCHEDULE (SCHEDULETITLE, SCHEDULEDATE) VALUES ('Rehearsal','2014-12-28');
								INSERT INTO TSCHEDULE (SCHEDULETITLE, SCHEDULEDATE) VALUES ('Glitz Glamour Copacabana - Hotel Borobudur','2014-12-31');
								INSERT INTO TSCHEDULE (SCHEDULETITLE, SCHEDULEDATE) VALUES ('Cover Karma Cinta','2015-01-11');
								INSERT INTO TSCHEDULE (SCHEDULETITLE, SCHEDULEDATE) VALUES ('Live Perform Acoustic at Permata Hijau','2015-01-24');
								
CREATE TABLE TPLAYLIST (
	PLAYLISTID INT NOT NULL AUTO_INCREMENT,
	PLAYLISTTITLE VARCHAR(255),
	PLAYLISTEMBED TEXT,
	PLAYLISTFEATURED INT,
	PLAYLISTTYPE INT,
	PLAYLISTDATE DATE,
	PRIMARY KEY (PLAYLISTID)
);

INSERT INTO TPLAYLIST (PLAYLISTTITLE, PLAYLISTEMBED, PLAYLISTFEATURED, PLAYLISTTYPE,  PLAYLISTDATE) VALUES ('APEL - KAU PUTUSKAN KU (KPK) GAME OVER (1st Single)','<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/179752698&amp;color=fc4fd5&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>','1','1','2015-01-01');
INSERT INTO TPLAYLIST (PLAYLISTTITLE, PLAYLISTEMBED, PLAYLISTFEATURED, PLAYLISTTYPE,  PLAYLISTDATE) VALUES ('APEL - Resahku (2nd single)','<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/181173241&amp;color=fc4fd5&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>','1','1','2015-01-01');
INSERT INTO TPLAYLIST (PLAYLISTTITLE, PLAYLISTEMBED, PLAYLISTFEATURED, PLAYLISTTYPE,  PLAYLISTDATE) VALUES ('APEL - Sedang Apa Kamu (3rd single)','<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/181173827&amp;color=fc4fd5&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>','1','1','2015-01-01');
INSERT INTO TPLAYLIST (PLAYLISTTITLE, PLAYLISTEMBED, PLAYLISTFEATURED, PLAYLISTTYPE,  PLAYLISTDATE) VALUES ('APEL (single)','<iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/63741769&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>','0','2','2015-01-01');
INSERT INTO TPLAYLIST (PLAYLISTTITLE, PLAYLISTEMBED, PLAYLISTFEATURED, PLAYLISTTYPE,  PLAYLISTDATE) VALUES ('APEL - GIRL POWER(album)','<iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/63738478&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>','0','2','2015-01-01');

CREATE TABLE TSTORY (
	STORYID INT NOT NULL AUTO_INCREMENT,
	STORYTITLE VARCHAR(255),
	STORYSLUG VARCHAR(255),
	STORYCONTENT TEXT,
	STORYIMAGES VARCHAR(255),
	STORYDATE DATE,
	PRIMARY KEY (STORYID)
);

INSERT INTO TSTORY (STORYTITLE, STORYSLUG, STORYCONTENT, STORYIMAGES, STORYDATE) VALUES ('Cerita Apel Manggung di Hari Ibu','cerita-apel-manggung-di-hari-ibu','Hari Ibu diperingati sebagai wujud penghargaan atas peran para ibu dan pengorbanan yang mereka berikan dalam keluarga maupun lingkungan sosial. Berbagai negara di dunia memperingati hari ibu di waktu yang berbeda sesuai dengan budaya masing-masing. Di Indonesia, hari penting tersebut diperingati setiap tanggal 22 Desember. Banyak hal positif dan unik yang bisa dilakukan untuk memperingati hari tersebut, mulai dari memberi kejutan dan kado, menggantikan pekerjaan ibu di rumah, memasak makanan favorit untuk ibu, mengadakan lomba untuk para ibu, mempersembahkan lagu untuk ibu, dll.<br><br>
Pada 22 Desember 2014, TVRI menayangkan konser musik khusus "Kasih Ibu" dan banyak menghadirkan musisi wanita seperti musisi senior Titiek Puspa, Dewi Yull, dan Melly Goeslaw. Selain sebagai musisi senior, mereka juga sebagai contoh wanita tangguh yang banyak menginspirasi para musisi wanita di Indonesia dan juga sebagai sosok ibu yang patut dicontoh. Selain itu, ada juga solois Shae serta band She dan Apel yang kesemua personelnya adalah wanita. <br><br>
Dalam acara yang ditayangkan mulai pukul 13.00 WIB tersebut, Apel membawakan single ke-3 "Sedang Apa Kamu" dan single terbaru "Karma Cinta". Lagu "Sedang Apa Kamu" bercerita tentang seseorang yang merindukan kekasihnya dan menunggu kabar dari orang tersebut. Sementara "Karma Cinta" bercerita tentang kekecewaan yang dirasakan seseorang atas hubungan yang dibangun di atas sebuah pengkhianatan. <br><br>
Apel sebagai salah satu band pengisi acara tersebut merasa sangat beruntung bisa menjadi bagian dari konser musik TVRI dan sepanggung dengan musisi-musisi tersebut. Sebagai calon ibu di masa yang akan datang, para personel Apel ingin menjadi ibu yang kreatif dan menuangkan kreatifitasnya dalam bermusik. Hal tersebut juga sebagai salah satu wujud bahwa calon ibu modern pada saat ini memiliki banyak peluang untuk berkreatifitas dalam segala bidang misalnya seni, pendidikan, olahraga, sosial, dll. <br><br>By: RA<br><br><br>','Mothers-Day-Flowers.jpg','2015-01-21');
*/
?>
