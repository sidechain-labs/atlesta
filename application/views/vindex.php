<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Atlesta Official Website</title>
    <meta name="Title" content="Atlesta Official Website">
	<meta name="Author" content="Atlesta">
	<meta name="Subject" content="Atlesta Official Website">
	<meta name="Description" content="Atlesta Official Website">
	<meta name="Keywords" content="Atlesta, atlesta, Sensation, sensation">
	<meta name="Language" content="Englsih">
	<meta name="Copyright" content="Copyright 2015, Atlesta, Powered by Sidechain Labs">
	<meta name="Designer" content="Atlesta, Sidechain Labs">
	<meta name="Publisher" content="Atlesta, Sidechain Labs">
	<meta name="Revisit-After" content="7 Days">
	<meta name="Distribution" content="Global">
	<meta name="Robots" content="Index">

	<link rel="shortcut icon" type="image/ico" href="<?php echo base_url();?>assets/img/favicon.png" />

	<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
	
	<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/hover.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
   
	<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

  </head>
  <body>
	
					
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 text-center">
				<br>
				<br>
				<div class="covers">
					<h5>THE BRAND NEW ALBUM</h5>
				</div>
				<img src="<?php echo base_url(); ?>assets/img/gestures.png" class="gestures">
				<div class="cover">
					<h5>AVAILABLE EVERYWHERE<br>JULY 17</h5>
				</div>
				<br>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-lg-offset-3 text-center">
				<div class="cover">
					<a href="#" class="hvr-outline-in"><img src="<?php echo base_url();?>assets/img/Atlesta Shovia.jpg" ></a>
				</div>
			</div>
			<div class="col-lg-3 text-center">
				<div class="cover">
					<img src="<?php echo base_url(); ?>assets/img/shovia.png" class="shovia">
					<h5>THE BRAND NEW SINGLE</h5>
					<a href="http://bit.ly/Shovia" target="_blank"><button class="btn-download" onClick="_gaq.push(['_trackEvent', 'Landing Page', 'click', 'Shovia Download iTunes']);">DOWNLOAD ITUNES</button></a>
					<a href="https://open.spotify.com/album/7uB6qhKDLNChUcvEr6rh4b" target="_blank"><button class="btn-download" onClick="_gaq.push(['_trackEvent', 'Landing Page', 'click', 'Shovia Stream Spotify']);">STREAM SPOTIFY</button></a>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 text-center">
				<div class="youtube">
					<iframe width="100%" height="315" src="https://www.youtube.com/embed/SCrkfvdt0Vg?rel=0&amp;showinfo=0" frameborder="4" allowfullscreen></iframe>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 text-center">
				<div class="entersite">
					<a href="<?php echo base_url();?>home/" class="hvr-pop" onClick="_gaq.push(['_trackEvent', 'Landing Page', 'Sensation Enter Site', 'Sensation Enter Site']);"><h3>ENTER SITE</h3></a>
				</div>
			</div>
		</div>
	</div>
	<?php $this->load->view('vfooter'); ?>
  
  </body>
</html>