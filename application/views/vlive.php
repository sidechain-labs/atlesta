<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Live | Atlesta Official Website</title>
    <meta name="Title" content="Atlesta Official Website">
	<meta name="Author" content="Atlesta">
	<meta name="Subject" content="Atlesta Official Website">
	<meta name="Description" content="Atlesta Official Website">
	<meta name="Keywords" content="Atlesta, atlesta, Sensation, sensation">
	<meta name="Language" content="Englsih">
	<meta name="Copyright" content="Copyright 2015, Atlesta, Powered by Sidechain Labs">
	<meta name="Designer" content="Atlesta, Sidechain Labs">
	<meta name="Publisher" content="Atlesta, Sidechain Labs">
	<meta name="Revisit-After" content="7 Days">
	<meta name="Distribution" content="Global">
	<meta name="Robots" content="Index">

	<link rel="shortcut icon" type="image/ico" href="<?php echo base_url();?>assets/img/favicon.png" />
	<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/hover.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

  </head>
  <body>
	<div class="container">
		<?php $this->load->view('vheader');?>
		<br><br>
		<div class="row">
			<div class="col-lg-10 col-lg-offset-1">
				<table class="table table-hover">
					<tr>
						<th>DATE</th>
						<th>DAY</th>
						<th>NAME</th>
						<th>LOCATION</th>
						<th>TICKETS</th>
						<th>RSVP</th>
					</tr>
					<?php foreach($live as $row):?>
					<tr>
						<td><?php echo date("d M Y",strtotime($row->LIVEDATE)); ?></td>
						<td><?php echo date("D",strtotime($row->LIVEDATE)); ?></td>
						<td><?php echo $row->LIVENAME; ?></td>
						<td><?php echo $row->LIVELOCATION; ?></td>
						<td><?php echo $row->LIVETICKETS; ?></td>
						<td><?php echo $row->LIVERSVP; ?></td>
					</tr>
					<?php endforeach; ?>
				</table>
			</div>
		</div>
	</div>
	<br><br>
	<?php $this->load->view('vfooter');?>
	<br>
    <script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/pinterest_grid.js"></script> 
	
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
  </body>
</html>