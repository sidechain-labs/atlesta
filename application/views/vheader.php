<div class="row">
			<div class="col-lg-10 col-lg-offset-1 text-center">
				<div class="logo">
					<p>
					<br><br><br>
					<a href="https://www.facebook.com/Atlesta" class="hvr-pop" target="_blank">FACEBOOK</a> &nbsp;&nbsp;//&nbsp;&nbsp; 
					<a href="https://twitter.com/atlesta" class="hvr-pop" target="_blank">TWITTER</a> &nbsp;&nbsp;//&nbsp;&nbsp; 
					<a href="http://instagram.com/atlesta" class="hvr-pop" target="_blank">INSTAGRAM</a> &nbsp;&nbsp;//&nbsp;&nbsp; 
					<a href="https://www.youtube.com/channel/UC3fnLGuoiVox0Tf9eCbze_g" class="hvr-pop" target="_blank">YOUTUBE</a> &nbsp;&nbsp;//&nbsp;&nbsp; 
					<a href="http://soundcloud.com/atlesta" class="hvr-pop" target="_blank">SOUNDCLOUD</a>			
					</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-10 col-lg-offset-1 text-center">
				<div class="logo">
					<a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/img/header-atlesta-2017.jpg"></a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-10 col-lg-offset-1 text-center">
				<div class="navigation">
					<a href="<?php echo base_url();?>home/" class="hvr-pop"><strong>HOME</strong></a> &nbsp;&nbsp;&nbsp;&nbsp; 
					<a href="<?php echo base_url();?>music/" class="hvr-pop"><strong>MUSIC</strong></a> &nbsp;&nbsp;&nbsp;&nbsp;
					<a href="<?php echo base_url();?>photo/" class="hvr-pop"><strong>PHOTO</strong></a> &nbsp;&nbsp;&nbsp;&nbsp;
					<a href="<?php echo base_url();?>video/" class="hvr-pop"><strong>VIDEO</strong></a> &nbsp;&nbsp;&nbsp;&nbsp;
					<a href="<?php echo base_url();?>store/" class="hvr-pop"><strong>STORE</strong></a> &nbsp;&nbsp;&nbsp;&nbsp;		
					<a href="<?php echo base_url();?>live/" class="hvr-pop"><strong>LIVE</strong></a> &nbsp;&nbsp;&nbsp;&nbsp;			
					<a href="<?php echo base_url();?>lyrics/" class="hvr-pop"><strong>LYRICS</strong></a>		
				</div>
			</div>
		</div>
		